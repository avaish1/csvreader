import csv
import codecs
from rest_framework.viewsets import  ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from concurrent.futures import ThreadPoolExecutor

from csvReader.models import UserInfo
from csvReader.serializers import UserInfoSerializer
from csvReader.utils import get_bulk_data_object


class UserInfoViewSet(ModelViewSet):
    queryset = UserInfo.objects.all()
    serializer_class = UserInfoSerializer

    @action(detail=False, methods=['POST'], serializer_class=UserInfoSerializer)
    def upload_data(self, request):
        max_worker = 4
        file = request.FILES.get("file")

        reader = csv.DictReader(codecs.iterdecode(file, "utf-8"), delimiter=",")
        data = list(reader)
        serializer = self.serializer_class(data=data, many=True)
        serializer.is_valid(raise_exception=True)
        user_list = get_bulk_data_object(data=serializer.data)
        # UserInfo.objects.bulk_create(user_list)     # without threading
        with ThreadPoolExecutor(max_workers=max_worker) as executor:
            executor.submit(UserInfo.objects.bulk_create, user_list)
        return Response("CSV data uploaded succesful", status=status.HTTP_201_CREATED)