import os
from rest_framework.test import RequestsClient, APITestCase
from django.test.client import MULTIPART_CONTENT, encode_multipart, BOUNDARY
from django.core.files.uploadedfile import SimpleUploadedFile

from csvReader.models import UserInfo


class RequestsClientTests(APITestCase):

    @classmethod
    def setUpClass(cls):
         UserInfo.objects.create(
            **{
                "name": "string",
                "address": "string",
                "contact": "852582258555",
                "email": "user1@example.com",
                "age": 12,
                "skills": "string",
                "profile": "string",
                "experience": "string",
                "gender": "male",
                "current_location": "string",
                "is_active": True,
            }
        )

    @classmethod
    def tearDownClass(cls):
        print('running after test')

    def test_user_get_request(self):
        client = RequestsClient()
        response = client.get('http://localhost:8001/user-info/')
        assert response.status_code == 200
        assert response.headers['Content-Type'] == 'application/json'

    def test_user_404_get_request(self):
        client = RequestsClient()
        response = client.get('http://localhost:8001/user-info1/')
        assert response.status_code == 404

    def test_user_post_request(self):
        client = RequestsClient()
        payload = {
          "name": "string",
          "address": "string",
          "contact": "852582258555",
          "email": "user12@example.com",
          "age": 12,
          "skills": "string",
          "profile": "string",
          "experience": "string",
          "gender": "male",
          "current_location": "string",
          "is_active": True,
        }
        response = client.post('http://localhost:8001/user-info/', json=payload)
        assert response.status_code == 201
        assert response.headers['Content-Type'] == 'application/json'

    def test_get_user_by_id_request(self):
        client = RequestsClient()
        response = client.get('http://localhost:8001/user-info/1/')
        assert response.status_code == 200
        assert response.headers['Content-Type'] == 'application/json'

    def test_get_user_by_wronge_id_request(self):
        client = RequestsClient()
        response = client.get('http://localhost:8001/user-info/-1/')
        assert response.status_code == 404
        assert response.headers['Content-Type'] == 'application/json'

    def test_user_patch_request(self):
        client = RequestsClient()
        payload = {
          "name": "string123",
          "address": "string",
          "contact": "852582258555",
          "email": "user1@example.com",
          "age": 12,
          "skills": "string",
          "profile": "string",
          "experience": "string",
          "gender": "male",
          "current_location": "string",
          "is_active": True,
        }
        response = client.patch('http://localhost:8001/user-info/1', json=payload)
        assert response.status_code == 200
        assert response.headers['Content-Type'] == 'application/json'

    def test_user_put_request(self):
        client = RequestsClient()
        payload = {
          "name": "string123",
        }
        response = client.put('http://localhost:8001/user-info/1', json=payload)
        assert response.status_code == 204
        assert response.headers['Content-Type'] == 'application/json'

    def test_user_put_request(self):
        client = RequestsClient()
        payload = {
          "name": "string123",
        }
        response = client.delete('http://localhost:8001/user-info/1', json=payload)
        assert response.status_code == 204

    def test_file_upload_csv(self):
        base_path = os.path.dirname(os.path.realpath(__file__))
        file = open(base_path + '/testcsv.csv', 'rb')

        file = SimpleUploadedFile(content=file.read(), name=file.name, content_type='multipart/form-data')
        client = RequestsClient()
        response = client.post('http://localhost:8001/user-info/upload_data/',
                               data=encode_multipart(data=dict(file=file, name="testcsv.csv"), boundary=BOUNDARY),
                               headers={'content_type': MULTIPART_CONTENT}
                               )
        print('line 121', response.status_code)
        assert response.status_code == 201