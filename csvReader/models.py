from django.db import models


class UserInfo(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contact = models.CharField(max_length=15)
    email = models.EmailField(max_length=255, unique=True)
    age = models.PositiveIntegerField()
    skills = models.CharField(max_length=255)
    profile = models.CharField(max_length=255)
    experience = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    current_location = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name} {self.profile}'
