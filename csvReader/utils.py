from csvReader.models import UserInfo

def get_bulk_data_object(data):
    user_list = []
    for row in data:
        user_list.append(
            UserInfo(
                name=row.get('name'),
                address=row.get('address'),
                contact=row.get('contact'),
                email=row.get('email'),
                age=row.get('age'),
                skills=row.get('skills'),
                profile=row.get('profile'),
                experience=row.get('experience'),
                gender=row.get('gender'),
                current_location=row.get('current_location'),
                is_active=row.get('is_active'),
            )
        )
    return user_list
